package ud7_arraylist_Hashtable;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Scanner;

public class Ex2 {

	
	
	public static void main(String[] args) {
		
		crearFactura();
	}

	private static void crearFactura() {
		
		Hashtable<String, Double> precioFinalProducto = new Hashtable<String, Double>();
		double precioFinal = 0;
		boolean salir = true;
		
		while(salir) {
			
			Scanner nombreProducto = new Scanner(System.in);
			System.out.print("Introduce el nombre del producto: ");	
			
			String nombreProductoIntro = nombreProducto.nextLine();
			
			Scanner cantidadProducto = new Scanner(System.in);
			System.out.print("Introduce la cantidad del producto: ");	
			
			int cantidadProductoIntro = cantidadProducto.nextInt();	

			Scanner precioProducto = new Scanner(System.in);
			System.out.print("Introduce el precio del producto: ");	
			
			double precioProductoIntro = precioProducto.nextDouble();	
			
			
			precioFinal = cantidadProductoIntro * precioProductoIntro;
			
			precioFinalProducto.put(nombreProductoIntro, precioFinal);		
			
			Scanner finalizarFactura = new Scanner(System.in);
			System.out.print("1) Introducer mas producto"
					+ "\n2) Finalizar factura"
					+ "\n3) No crear factura"
					+ "\nIntroduce una opci�n: ");
			
			int finalizarFacturaIntro = finalizarFactura.nextInt();	
			
			switch (finalizarFacturaIntro) {
			case 1:
				salir = true;
				break;
			case 2:
				salir = false;
				finalizarCompra(precioFinalProducto);
				break;
				
			default:
				System.out.print("FIN del programa!! ");	
				salir = false;
				break;
			}
		}
		
		
	}
	
	private static void finalizarCompra(Hashtable<String, Double> precioFinalProducto) {
		double precioFinal = 0;
		double precioFinaltTotal = 0;
		double devolverCantidad = 0;
		for (Double val : precioFinalProducto.values()){
			precioFinal += val;
		}
		
		precioFinalProducto.values();
		
		Scanner finalizarFactura = new Scanner(System.in);
		System.out.print("1) Aplicar IVA 21%"
				+ "\n2)Aplicar IVA 4%"
				+ "\nIntroduce una opci�n: ");
		
		int finalizarFacturaIntro = finalizarFactura.nextInt();	
		
		switch (finalizarFacturaIntro) {
		case 1:
			precioFinaltTotal = (precioFinal * 0.21) + precioFinal;
			break;
		case 2:
			precioFinaltTotal = (precioFinal * 0.4) + precioFinal;
			break;
			
		default:
			break;
		}
		System.out.println("Precio Final a apagar: "+precioFinaltTotal);
		Scanner cantidadPagada = new Scanner(System.in);
		System.out.print("Introduce la cantidad a apagar: ");
		
		double cantidadPagadaIntro = finalizarFactura.nextDouble();	
		
		devolverCantidad = cantidadPagadaIntro - precioFinaltTotal ;
		
		System.out.println("************* FIN DE LA APLICACION *************");
		System.out.println("IVA aplicado "+ ((finalizarFacturaIntro == 1) ? "21" : "4") +"%");
		System.out.println("Precio total bruto: "+precioFinal+"\n precio mas IVA: "+precioFinaltTotal);
		System.out.println("N�mero de articulos comprados: "+precioFinalProducto.size());
		System.out.println("Cantidad pagada: "+cantidadPagadaIntro);
		System.out.println("Cambio a devolver al cliente: "+devolverCantidad);
		
		
	}

	
	

}
