package ud7_arraylist_Hashtable;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

public class Ex1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner valor = new Scanner(System.in); 
		System.out.print("Introduce la cantidad de alumnos en la clase de programación: ");
		
		int alumnosIntro = valor.nextInt();
		calculamosLaNotaMedia(alumnosIntro);
		
		
	}

	private static void calculamosLaNotaMedia(int alumnosIntro) {
		ArrayList<Integer> listaAlumnos = new ArrayList();
		
		Hashtable<String, String> notaMediaAlumnos = new Hashtable<String, String>();
		
		for (int i = 1; i <= alumnosIntro; i++) {
			
			double calcularNotaMedia = 0;
			
			System.out.println("");
			System.out.println("----- Introducción de los datos del "+i+" alumno -----");
			
			Scanner nombreAlumno = new Scanner(System.in); 
			System.out.print("Introduce el nombre del alumno "+i+": ");
			
			String nombreAlumnoIntro = nombreAlumno.nextLine();
			
			Scanner notaPrimerTrimestre = new Scanner(System.in); 
			System.out.print("Introduce la nota del primer tremistre al alumno "+i+": ");
			
			Double notaPrimerTrimestreIntro = notaPrimerTrimestre.nextDouble();
			
			Scanner notaSegundoTrimestre = new Scanner(System.in); 
			System.out.print("Introduce la nota del segundo tremistre al alumno "+i+": ");
			
			double notaSegundoTrimestreIntro = notaSegundoTrimestre.nextDouble();
			
			Scanner notaTrecerTrimestre = new Scanner(System.in); 
			System.out.print("Introduce la nota del trecer tremistre al alumno "+i+": ");
			
			double notaTrecerTrimestreIntro = notaTrecerTrimestre.nextDouble();	
			
			calcularNotaMedia = (notaPrimerTrimestreIntro +  notaSegundoTrimestreIntro +notaTrecerTrimestreIntro) / 3;
			
			notaMediaAlumnos.put(nombreAlumnoIntro, String.valueOf(calcularNotaMedia)); //Guardamos los datos en un dicciónario.
		}
		
	}

}
